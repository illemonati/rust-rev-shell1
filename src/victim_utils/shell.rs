use tokio::process::{Command, Child};
use std::process::Stdio;
use tokio::io;

#[cfg(target_os = "windows")]
pub fn spawn_shell() -> io::Result<Child>{
    Command::new("cmd")
        .arg("/c")
        .arg("cmd")
        .stdout(Stdio::piped())
        .stdin(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
}

#[cfg(not(target_os = "windows"))]
pub fn spawn_shell() -> io::Result<Child> {
    Command::new("/bin/sh")
        .stdout(Stdio::piped())
        .stdin(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
}