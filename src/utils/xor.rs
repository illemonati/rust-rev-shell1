
pub fn xor(data: impl Into<Vec<u8>>, key: &str) -> Vec<u8> {
    let mut res = data.into();
    xor_in_place(&mut res, key);
    res
}

pub fn xor_in_place(data: &mut [u8], key: &str) {
    if !data.is_empty() {
        for k in key.as_bytes() {
            xor_single_inplace(data, *k)
        }
    }
}

pub fn xor_single_inplace(data: &mut [u8], c: u8) {
    if !data.is_empty() {
        for b in data {
            *b ^= c;
        }
    }
}

