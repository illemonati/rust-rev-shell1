use crate::utils::xor::{xor_in_place};
use std::u8;
use crate::utils::configs::ADD_AMOUNT;

pub fn encrypt_str(data: &str, key: &str) -> Vec<u8> {
    encrypt(data, key)
}

pub fn decrypt_str(data: &[u8], key: &str) -> String {
    String::from_utf8_lossy(&decrypt(data, key)).to_string()
}


pub fn encrypt(data: impl Into<Vec<u8>>, key: &str) -> Vec<u8> {
    let mut res = data.into();
    encrypt_in_place(&mut res, key);
    res
}

pub fn decrypt(data: impl Into<Vec<u8>>, key: &str) -> Vec<u8> {
    let mut res = data.into();
    decrypt_in_place(&mut res, key);
    res
}

pub fn encrypt_in_place(data: &mut [u8], key: &str) {
    xor_in_place(data, key);
    for b in data {
        *b = u8::MAX - *b;
        *b = b.wrapping_add(ADD_AMOUNT);
    }
}


pub fn decrypt_in_place(data: &mut [u8], key: &str) {
    for b in data.iter_mut() {
        *b = b.wrapping_sub(ADD_AMOUNT);
        *b = u8::MAX - *b;
    }
    xor_in_place(data, key);
}